'use strict';

import gulp from 'gulp';
import requireDir from 'require-dir';
import runSequence from 'run-sequence';

// Подключаем все таски
requireDir( './lib/tasks', {
  recurse: true
});

gulp.task('build', ['html', 'styles', 'images', 'fonts', 'scripts']);

gulp.task('default', () => {
  runSequence('clean', 'build', 'browser-sync', 'watch');
});