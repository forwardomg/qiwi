var isOperaMini = Object.prototype.toString.call(window.operamini) === "[object OperaMini]"

if (!isOperaMini) {
  $(document).ready(function() {
    var button = (function() {

      var $el = $('#button'),
          $elFixed = $('#button-fixed'),
          elFromTop = $el.offset().top;

      $elFixed.addClass('button_fixed');

      return {
        toggle: function() {
          var fromTop = $(document).scrollTop();
          $elFixed.toggleClass('button_show-up', fromTop > elFromTop);
        },
        handlers: function() {
          var that = this;
          $(document).on('scroll', function() {
            that.toggle();
          });
        },

        init: function() {
          this.handlers();
          this.toggle();
        }
      }

    }());

    button.init();
  });
} else {
  document.body.className = 'opera-mini';
}