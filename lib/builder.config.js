'use strict';

import util from 'gulp-util';

let isProd = !!util.env.production;

const dirs = {
  src: `./src`,
  dest: `./dest`,
  build:  isProd ? `./dest/build` : `./dest/unbuild`
};

module.exports = {
  paths: {
    dirs,
    src: {
      html: `${dirs.src}/index.html`,
      styles: `${dirs.src}/styles/main.scss`,
      images: `${dirs.src}/images/*`,
      fonts: `${dirs.src}/fonts/*`,
      scripts: `${dirs.src}/javascript/*`
    },
    dest: {
      html: `${dirs.build}`,
      styles: `${dirs.build}/styles`,
      images: `${dirs.build}/images`,
      fonts: `${dirs.build}/fonts`,
      scripts: `${dirs.build}/javascript`
    }
  }
};
