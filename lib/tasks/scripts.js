'use strict';

import gulp from 'gulp';
import options from '../builder.config';

gulp.task('scripts', () =>
  gulp.src(options.paths.src.scripts)
    .pipe(gulp.dest(options.paths.dest.scripts))
);