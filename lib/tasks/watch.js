import gulp from 'gulp';

import options from '../builder.config';

gulp.task('watch', () => {
  const glob = [`${options.paths.dirs.src}/**/*.{html,scss,js}`];

  gulp.watch(glob, ['html', 'styles', 'scripts']);

  gulp.watch(glob, global.browserSync.reload);
});