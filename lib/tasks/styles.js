'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import csso from 'gulp-csso';
import sourcemaps from 'gulp-sourcemaps';
import util from 'gulp-util';
import gulpif from 'gulp-if';

import options from '../builder.config';

let isProd = !!util.env.production;

gulp.task('styles', () => {
  return gulp.src(options.paths.src.styles)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulpif(isProd, csso()))
    .pipe(gulp.dest(options.paths.dest.styles));
});