'use strict';

import gulp from 'gulp';
import htmlmin from 'gulp-htmlmin';
import util from 'gulp-util';
import gulpif from 'gulp-if';

import options from '../builder.config';

let isProd = !!util.env.production;

gulp.task('html', () =>
  gulp.src(options.paths.src.html)
    .pipe(gulpif(isProd, htmlmin({
      collapseWhitespace: true,
      conservativeCollapse: true
    })))
    .pipe(gulp.dest(options.paths.dest.html))
);