'use strict';

import gulp from 'gulp';

import options from '../builder.config';

gulp.task('fonts', () =>
  gulp.src(options.paths.src.fonts)
    .pipe(gulp.dest(options.paths.dest.fonts))
);