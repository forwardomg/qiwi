'use strict';

import gulp from 'gulp';
import imagemin from 'gulp-imagemin';

import options from '../builder.config';

gulp.task('images', () =>
  gulp.src(options.paths.src.images)
    .pipe(imagemin([
      imagemin.svgo({plugins: [{removeViewBox: true}]})
    ]))
    .pipe(gulp.dest(options.paths.dest.images))
);