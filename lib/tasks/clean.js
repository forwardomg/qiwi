'use strict';

import gulp from 'gulp';
import clean from 'gulp-clean';

import options from '../builder.config';

gulp.task('clean', function () {
  return gulp.src(options.paths.dirs.dest, {read: false})
    .pipe(clean());
});