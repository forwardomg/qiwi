'use strict';

import gulp from 'gulp';
import {create as bsCreate} from 'browser-sync';

import options from '../builder.config';

global.browserSync = bsCreate();

// Serve files from the root of this project
gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: options.paths.dirs.build
    },
    reloadDelay: 1000
  });
});